package ViewModel

import Model.Marker
import android.net.Uri
import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng

class MarkerViewModel: ViewModel() {
    val mark= mutableListOf<Marker>()
    var latitud= 0.0
    var longitud= 0.0
    var image: Uri?= null
    var currentUser: Marker? = null
    var userName= ""
    var usermail= ""
    var tittle= ""
    var currentLatitud=0.0
    var currentLongitud= 0.0
    var nameUpdated= ""
    fun add(latitud: Double, longitud: Double, image: String, tittle: String){
        mark.add(Marker(latitud,longitud,tittle,image))
    }
    fun setSelectedMarker(marker: Marker){
        currentUser = marker
    }
}