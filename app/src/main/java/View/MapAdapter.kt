package View

import Model.Marker
import Model.MyOnClickListener
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.damaps.R
import com.example.damaps.databinding.MarkerDesignBinding
import com.google.firebase.storage.FirebaseStorage
import okhttp3.internal.notify
import java.io.File

class MapAdapter(private var markers: MutableList<Marker>, private val listener: MyOnClickListener):
    RecyclerView.Adapter<MapAdapter.ViewHolder>() {
    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = MarkerDesignBinding.bind(view)
        fun setListener(marker: Marker){
            binding.detail.setOnClickListener {
                listener.detail(marker)
            }
            binding.onMap.setOnClickListener{
                listener.onMap(marker)
            }
            binding.delete.setOnClickListener {
                listener.delete(marker)
            }
        }
    }
    private lateinit var context: Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.marker_design, parent, false)
        return ViewHolder(view)
    }
    override fun getItemCount(): Int {
        return markers.size
    }
    fun setMarkerList(markerList: MutableList<Marker>){
        this.markers = markerList
        notifyDataSetChanged()
    }
    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val marker = markers[position]
        with(holder){
            setListener(marker)
            binding.description.text = marker.tittle
            val storage = FirebaseStorage.getInstance().reference.child("images/${marker.Photo}")
            val localFile = File.createTempFile("temp", "jpeg")
            storage.getFile(localFile).addOnSuccessListener {
                val bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
                binding.image.setImageBitmap(bitmap)
                binding.image.rotation = 90f

            }
        }
    }
}