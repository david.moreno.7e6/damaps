package View

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import com.example.damaps.R
import com.example.damaps.databinding.FragmentLaunchScreenBinding
import com.google.firebase.auth.FirebaseAuth

class LaunchScreen : Fragment() {
    lateinit var binding: FragmentLaunchScreenBinding
    lateinit var myPreferences: SharedPreferences
    lateinit var timer: CountDownTimer
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding= FragmentLaunchScreenBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        myPreferences =
            requireActivity().getSharedPreferences("MySharedPreferences", Context.MODE_PRIVATE)
        val email = myPreferences.getString("email", "")
        val password = myPreferences.getString("password", "")
        if (email != "" && password != "") {
            FirebaseAuth.getInstance().signInWithEmailAndPassword(email!!, password!!)
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        timer = object : CountDownTimer(3000, 1000) {
                            override fun onTick(millisUntilFinished: Long) {
                                binding.countdown.progress += 1
                            }

                            override fun onFinish() {
                                val emailLogged = it.result?.user?.email
                                val action =
                                    LaunchScreenDirections.actionLaunchScreenToMapFragment(emailLogged!!)
                                findNavController().navigate(action)
                            }

                        }.start()
                    } else {
                        Toast.makeText(
                            requireContext(),
                            "Incorrect Email or Password",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
        }
        else {
            timer = object : CountDownTimer(3000, 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    binding.countdown.progress += 1
                }

                override fun onFinish() {
                    val action = LaunchScreenDirections.actionLaunchScreenToLoginFragment()
                    findNavController().navigate(action)
                }

            }.start()
        }
    }
    override fun onResume() {
        super.onResume()
        val supportActionBar: ActionBar? = (requireActivity() as AppCompatActivity).supportActionBar
        if (supportActionBar != null) supportActionBar.hide()
    }

    override fun onStop() {
        super.onStop()
        val supportActionBar: ActionBar? = (requireActivity() as AppCompatActivity).supportActionBar
        if (supportActionBar != null) supportActionBar.show()
    }

}