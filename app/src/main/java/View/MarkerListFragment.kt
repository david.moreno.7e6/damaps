package View

import Model.Marker
import Model.MyOnClickListener
import Model.User
import ViewModel.MarkerViewModel
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.damaps.R
import com.example.damaps.databinding.FragmentMarkerListBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.*


class MarkerListFragment : Fragment(), MyOnClickListener {
    private val auth= FirebaseAuth.getInstance()
    lateinit var binding: FragmentMarkerListBinding
    private lateinit var mapAdapter: MapAdapter
    private val db = FirebaseFirestore.getInstance()
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private val viewModel: MarkerViewModel by activityViewModels()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding= FragmentMarkerListBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        setUpRecyclerView(viewModel.mark)
        setUpRecyclerView(mutableListOf<Marker>())
        eventChangeListener()

    }
    private fun eventChangeListener() {
        var markerList = arrayListOf<Marker>()
        db.collection("users").document(auth.currentUser?.email!!).collection("marcadores").addSnapshotListener(object: EventListener<QuerySnapshot> {
            override fun onEvent(value: QuerySnapshot?, error: FirebaseFirestoreException?) {
                if(error != null){
                    Log.e("Firestore error", error.message.toString())
                    return
                }
                for(dc: DocumentChange in value?.documentChanges!!){
                    if(dc.type == DocumentChange.Type.ADDED){
                        var values= dc.document.getData()
                        val newMarker = Marker(values["latitud"] as Double,
                            values["longitud"] as Double, values["tittle"] as String,
                            values["Photo"] as String
                        )
                        newMarker.tittle = dc.document.id
                        markerList.add(newMarker)
                    }
                }
                mapAdapter.setMarkerList(markerList)
            }
        })
    }

    override fun detail(marker: Marker) {
        viewModel.setSelectedMarker(marker)
        findNavController().navigate(R.id.action_markerListFragment_to_detailFragment)
    }

    override fun onMap(marker: Marker) {
        viewModel.setSelectedMarker(marker)
        viewModel.latitud = marker.latitud
        viewModel.longitud= marker.longitud
        findNavController().navigate(MarkerListFragmentDirections.actionMarkerListFragmentToMapFragment(viewModel.usermail))
    }

    override fun delete(marker: Marker) {
        viewModel.setSelectedMarker(marker)
        db.collection("users").document(auth.currentUser?.email!!).collection("marcadores").document(marker.tittle).delete()
        findNavController().navigate(MarkerListFragmentDirections.actionMarkerListFragmentSelf())
        }
    private fun setUpRecyclerView(listOfMarkers: MutableList<Marker>){
        mapAdapter = MapAdapter(listOfMarkers , this)
        linearLayoutManager = LinearLayoutManager(context)
        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = mapAdapter
        }
    }

}