package View

import ViewModel.MarkerViewModel
import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.location.LocationRequestCompat
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.damaps.R
import com.example.damaps.databinding.FragmentMarkerBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import java.text.SimpleDateFormat
import java.util.*


class MarkerFragment : Fragment() {
    private val db = FirebaseFirestore.getInstance()
    private val auth= FirebaseAuth.getInstance()
    lateinit var binding: FragmentMarkerBinding
    private val viewModel: MarkerViewModel by activityViewModels()
    lateinit var image: Uri
    var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            result ->
        if (result.resultCode == RESULT_OK) {
            val data: Intent? = result.data
            if (data != null) {
                val imageUri = data.data!!
                image = imageUri
                viewModel.image = imageUri
                binding.image.setImageURI(imageUri)
            }
        }
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMarkerBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (viewModel.image != null){
            binding.image.setImageURI(viewModel.image)
        }
        else {
            binding.image.setImageResource(R.drawable.foto)
        }

        if (viewModel.latitud == 0.0 && viewModel.longitud == 0.0){
            viewModel.latitud = viewModel.currentLatitud
            viewModel.longitud= viewModel.currentLongitud
        }
        binding.tittlevalue.setText(viewModel.tittle)

        binding.cameraAccess.setOnClickListener {
            findNavController().navigate(R.id.action_markerFragment_to_cameraFragment)
            viewModel.tittle= binding.tittlevalue.text.toString()
        }
        binding.galeryAccess.setOnClickListener {
            selectImage()
        }


        binding.add.setOnClickListener {
            if (binding.tittlevalue.text.toString() == ""){
                Toast.makeText(requireContext(), "You cannot add marker without tittle", Toast.LENGTH_SHORT).show()
            }
            else{
                if(viewModel.nameUpdated != ""){
                    db.collection("users").document(auth.currentUser?.email!!).collection("marcadores").document(viewModel.nameUpdated).delete()
                }
                viewModel.tittle= binding.tittlevalue.text.toString()
                val formatter = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.getDefault())
                val now = Date()
                val fileName = formatter.format(now)
                val storage = FirebaseStorage.getInstance().getReference("images/$fileName")
                storage.putFile(viewModel.image!!)
                    .addOnSuccessListener {
                        binding.image.setImageURI(null)
                        db.collection("users").document(auth.currentUser?.email!!).collection("marcadores").document(binding.tittlevalue.text.toString()).set(
                            hashMapOf("tittle" to binding.tittlevalue.text.toString(), "Photo" to fileName, "latitud" to viewModel.latitud, "longitud" to viewModel.longitud)
                        )
                        Toast.makeText(requireContext(), "Image uploaded!", Toast.LENGTH_SHORT).show()
                        viewModel.latitud= 0.0
                        viewModel.longitud= 0.0
                        viewModel.image = null
                        viewModel.tittle= ""
                        findNavController().navigate(R.id.action_markerFragment_to_mapFragment)
                    }
                    .addOnFailureListener {
                        Toast.makeText(requireContext(), "Image not uploaded!", Toast.LENGTH_SHORT).show()
                    }

            }
        }

    }
    private fun selectImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        resultLauncher.launch(intent)
    }
}

