package View

import ViewModel.MarkerViewModel
import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnLongClickListener
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.damaps.R
import com.example.damaps.databinding.FragmentDetailBinding
import com.example.damaps.databinding.FragmentMapBinding
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

const val REQUEST_CODE_LOCATION = 100

class MapFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMapLongClickListener{
    lateinit var binding: FragmentMapBinding
    lateinit var map: GoogleMap
    private val auth= FirebaseAuth.getInstance()
    private val db = FirebaseFirestore.getInstance()
    lateinit var options: GoogleMapOptions
    private val viewModel: MarkerViewModel by activityViewModels()
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var currentCoordinates: LatLng
    lateinit var radioGroup: RadioGroup
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding= FragmentMapBinding.inflate(layoutInflater)
        radioGroup= binding.maptypes
        fusedLocationProviderClient =  LocationServices.getFusedLocationProviderClient(requireActivity())
        getLocation()
        userReady()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.capa.setOnClickListener {
            binding.maptypes.visibility = View.VISIBLE
        }
        binding.ok.setOnClickListener {
            checkMapType()
        }
    }
    fun userReady(){
        val email = arguments?.getString("email").toString()
        db.collection("users").document(email!!).get().addOnSuccessListener{
            val navigationView = requireActivity().findViewById(com.example.damaps.R.id.navigationView) as NavigationView
            val headerView = navigationView.getHeaderView(0)
            val navUsername = headerView.findViewById<View>(R.id.username) as TextView
            if (viewModel.userName == ""){
                viewModel.userName= it.get("name") as String
            }
            if(viewModel.usermail== ""){
                viewModel.usermail= email
            }
            val navUseremail = headerView.findViewById<View>(R.id.usermail) as TextView
            navUsername.setText(viewModel.userName)
            navUseremail.text = viewModel.usermail
        }
    }
    fun createMap(){
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as    SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.setOnMapLongClickListener(this)
        map.uiSettings.isZoomControlsEnabled = true
        map.uiSettings.isMapToolbarEnabled = true
        createMarker()
        if(viewModel.latitud != 0.0 && viewModel.longitud != 0.0){
            locateMarker()
        }
        enableLocation()
    }
    fun createMarker(){
        db.collection("users").document(auth.currentUser?.email!!).collection("marcadores").get().addOnSuccessListener {
            for (document in it) {
                val coordinates= LatLng(document["latitud"] as Double,document["longitud"] as Double)
                val myMarker = MarkerOptions().position(coordinates).title(document["tittle"] as String?)
                map.addMarker(myMarker)
            }
        }
    }
    fun locateMarker(){
        currentCoordinates= LatLng(viewModel.latitud,viewModel.longitud)
        map.animateCamera(
            CameraUpdateFactory.newLatLngZoom(currentCoordinates, 18f),
            5000, null)
    }
    private fun isLocationPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }
    @SuppressLint("MissingPermission")
    private fun enableLocation(){
        if(!::map.isInitialized) return
        if(isLocationPermissionGranted()){
            map.isMyLocationEnabled = true
        }
        else{
            requestLocationPermission()
        }
    }
    private fun requestLocationPermission(){
        if(ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)){
            Toast.makeText(requireContext(), "Ves a la pantalla de permisos de l’aplicació i habilita el de Geolocalització", Toast.LENGTH_SHORT).show()
        }
        else{
            ActivityCompat.requestPermissions(requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE_LOCATION)
        }
    }
    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        when(requestCode){
            REQUEST_CODE_LOCATION -> if(grantResults.isNotEmpty() &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED){
                map.isMyLocationEnabled = true
                getLocation()
            }
            else{
                Toast.makeText(requireContext(), "Accepta els permisos de geolocalització",
                    Toast.LENGTH_SHORT).show()
            }
        }
    }
    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        if(!::map.isInitialized) return
        if(!isLocationPermissionGranted()){
            map.isMyLocationEnabled = false
            Toast.makeText(requireContext(), "Accepta els permisos de geolocalització",
                Toast.LENGTH_SHORT).show()
        }
    }
    @SuppressLint("MissingPermission")
    private fun getLocation() {
        if (isLocationPermissionGranted()) {
            fusedLocationProviderClient.lastLocation.addOnCompleteListener {
                val location = it.result
                if (location != null) {
                    currentCoordinates = LatLng(location.latitude, location.longitude)
                    viewModel.currentLatitud= location.latitude
                    viewModel.currentLongitud= location.longitude
                    createMap()
                }
            }
        } else {
            requestLocationPermission()
        }
    }


    override fun onMapLongClick(p0: LatLng) {
        viewModel.longitud= p0.longitude
        viewModel.latitud= p0.latitude
        viewModel.image= null
        findNavController().navigate(R.id.action_mapFragment_to_markerFragment)
    }

    fun checkMapType(){
        when(radioGroup.checkedRadioButtonId){
            R.id.hybrid-> {
                map.mapType= GoogleMap.MAP_TYPE_HYBRID
                binding.maptypes.visibility = View.INVISIBLE
                createMap()
            }
            R.id.satellite-> {
                map.mapType= GoogleMap.MAP_TYPE_SATELLITE
                binding.maptypes.visibility = View.INVISIBLE
                createMap()
            }
            R.id.normal-> {
                map.mapType= GoogleMap.MAP_TYPE_NORMAL
                binding.maptypes.visibility = View.INVISIBLE
                createMap()
            }
            R.id.terrain-> {
                map.mapType= GoogleMap.MAP_TYPE_TERRAIN
                binding.maptypes.visibility = View.INVISIBLE
                createMap()
            }
        }
    }

}
