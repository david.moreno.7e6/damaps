package View

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.provider.Settings.Global.putString
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.edit
import androidx.navigation.fragment.findNavController
import com.example.damaps.R
import com.example.damaps.databinding.FragmentLoginBinding
import com.google.firebase.auth.FirebaseAuth
import okhttp3.internal.cache2.Relay.Companion.edit

class LoginFragment : Fragment() {
    lateinit var binding: FragmentLoginBinding
    lateinit var myPreferences: SharedPreferences
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding= FragmentLoginBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.register.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }
        myPreferences =
            requireActivity().getSharedPreferences("MySharedPreferences", Context.MODE_PRIVATE)
        binding.login.setOnClickListener {
            if(binding.switcher.isChecked){
                println("HOLA")
                myPreferences.edit {
                    putString("email", binding.emailValue.text.toString())
                    putString("password", binding.password.text.toString())
                }
            }
            FirebaseAuth.getInstance().
            signInWithEmailAndPassword(binding.emailValue.text.toString(), binding.password.text.toString())
                .addOnCompleteListener {
                    if(it.isSuccessful){
                        val emailLogged = it.result?.user?.email
                        val action = LoginFragmentDirections.actionLoginFragmentToMapFragment(emailLogged!!)
                        findNavController().navigate(action)
                    }
                    else{
                        Toast.makeText(requireContext(), "Incorrect Email or Password", Toast.LENGTH_SHORT).show()
                    }
                }
        }
    }
    override fun onResume() {
        super.onResume()
        val supportActionBar: ActionBar? = (requireActivity() as AppCompatActivity).supportActionBar
        if (supportActionBar != null) supportActionBar.hide()
    }

    override fun onStop() {
        super.onStop()
        val supportActionBar: ActionBar? = (requireActivity() as AppCompatActivity).supportActionBar
        if (supportActionBar != null) supportActionBar.show()
    }

}