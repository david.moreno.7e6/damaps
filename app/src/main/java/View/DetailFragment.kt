package View

import ViewModel.MarkerViewModel
import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.damaps.R
import com.example.damaps.databinding.FragmentDetailBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import java.io.File


class DetailFragment : Fragment() {

    lateinit var binding: FragmentDetailBinding
    private val viewModel: MarkerViewModel by activityViewModels()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.latitudeValue.text = viewModel.currentUser?.latitud.toString()
        binding.longitudeValue.text = viewModel.currentUser?.longitud.toString()
        binding.nameValue.text= viewModel.currentUser?.tittle
        val storage = FirebaseStorage.getInstance().reference.child("images/${viewModel.currentUser?.Photo}")
        val localFile = File.createTempFile("temp", "jpeg")
        storage.getFile(localFile).addOnSuccessListener {
            val bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
            binding.imageDetail.setImageBitmap(bitmap)
            binding.imageDetail.rotation = 90f

        }.addOnFailureListener{
            Toast.makeText(requireContext(), "Error downloading image!", Toast.LENGTH_SHORT)
                .show()
        }

        binding.editar.setOnClickListener {
            viewModel.latitud= viewModel.currentUser?.latitud!!
            viewModel.longitud= viewModel.currentUser?.longitud!!
            viewModel.nameUpdated= viewModel.currentUser?.tittle!!
            findNavController().navigate(DetailFragmentDirections.actionDetailFragmentToMarkerFragment())
        }

    }

}