package View

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import com.example.damaps.R
import com.example.damaps.databinding.FragmentRegisterBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore


class RegisterFragment : Fragment() {

    lateinit var binding: FragmentRegisterBinding
    private val db = FirebaseFirestore.getInstance()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentRegisterBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {3
        super.onViewCreated(view, savedInstanceState)
        binding.signIn.setOnClickListener {
            FirebaseAuth.getInstance().
            createUserWithEmailAndPassword(binding.emailValue.text.toString(), binding.password.text.toString())
                .addOnCompleteListener {
                    if(it.isSuccessful){
                        val emailLogged = it.result?.user?.email
                        db.collection("users").document(emailLogged!!).set(
                            hashMapOf("name" to binding.nameRegister.text.toString(),)
                        )
                        val action = RegisterFragmentDirections.actionRegisterFragmentToMapFragment(emailLogged)
                        findNavController().navigate(action)
                    }
                    else{
                        Toast.makeText(requireContext(), "User registration error", Toast.LENGTH_SHORT).show()
                    }
                }
        }
    }
    override fun onResume() {
        super.onResume()
        val supportActionBar: ActionBar? = (requireActivity() as AppCompatActivity).supportActionBar
        if (supportActionBar != null) supportActionBar.hide()
    }

    override fun onStop() {
        super.onStop()
        val supportActionBar: ActionBar? = (requireActivity() as AppCompatActivity).supportActionBar
        if (supportActionBar != null) supportActionBar.show()
    }
}