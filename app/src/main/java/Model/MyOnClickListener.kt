package Model

interface MyOnClickListener {
    fun detail(marker: Marker)
    fun onMap(marker: Marker)
    fun delete(marker: Marker)
}